import { Controller, Get, HttpStatus, HttpCode, Param, Post, Body, UseGuards, Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDTO } from '../models/create-user.dto';
import { ShowUserInListDTO } from '../models/show-user-in-list.dto';
import { AuthGuardWithBlacklisting } from '../common/guards/auth-blacklist.guard';
import { userDecorator } from '../common/decorators/user.decorator';
import { ShowUserDTO } from '../models/show-user.dto';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Get()
    @HttpCode(HttpStatus.OK)
    public async getAllUsers(): Promise<ShowUserInListDTO[]> {
        return await this.usersService.getAllUsers();
    }

    // @Get('hi')
    // @HttpCode(HttpStatus.OK)
    // @UseGuards(AuthGuardWithBlacklisting)
    // public async sayHi(@userDecorator('user') loggedUser: ShowUserDTO) {
    //     return `Hi ${loggedUser.username}`;
    // }

    @Get(':id')
    @HttpCode(HttpStatus.OK)
    public async showsingleUser(
        @Param('id') userId: string
        ): Promise<ShowUserInListDTO> {
        return await this.usersService.getUserById(userId);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    public async addNewUser(@Body() newUser: CreateUserDTO): Promise<ShowUserInListDTO> {
        return await this.usersService.createUser(newUser);
    }
}
