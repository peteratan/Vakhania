import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDTO } from '../models/create-user.dto';
import * as bcrypt from 'bcrypt';
import { ShowUserInListDTO } from '../models/show-user-in-list.dto';
import { ShowUserDTO } from '../models/show-user.dto';
import { AuthUserDTO } from '../models/auth-user.dto';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>
    ) {}

    public async getAllUsers(): Promise<ShowUserInListDTO[]> {
        const foundUsers = await this.userRepo.find();
    
        return foundUsers;
      }
    
      public async getUserById(userId: string): Promise<ShowUserInListDTO> {
        const foundUser = await this.userRepo.findOne({
          where: {id: userId}
        });
    
        if (!foundUser) {
          throw new Error('No such user found!');
        }
    
        return foundUser;
      }

      public async findUser(credential: string): Promise<ShowUserDTO> {
        const foundUser = await this.userRepo.findOne({
          where: [{username: credential}]
        });
    
        if (!foundUser) {
          throw new Error('No such user found!');
        }
    
        return foundUser;
      }

      public async validateUserPassword(user: AuthUserDTO): Promise<boolean> {
        const userEntity: User = await this.userRepo.findOne({
          where: [{
              username: user.credential
            }
          ],
        });
    
        return await bcrypt.compare(user.password, userEntity.password);
      }

      public async createUser(userToCreate: CreateUserDTO): Promise<ShowUserInListDTO> {
        const foundUser: User = await this.userRepo.findOne({
          where: [
                    {username: userToCreate.username}
          ],
        });
    
        if (foundUser) {
          throw new Error('User with such username/email already exists!');
        }
    
        const newUser: User = this.userRepo.create(userToCreate);
        newUser.password = await bcrypt.hash(userToCreate.password, 10);

        return await this.userRepo.save(newUser);
      }
}
