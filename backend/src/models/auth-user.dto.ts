export class AuthUserDTO {
    public credential: string;
    public password: string;
}