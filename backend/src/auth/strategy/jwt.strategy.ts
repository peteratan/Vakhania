import { UsersService } from '../../users/users.service';
import { ConfigService } from '../../config/config.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ShowUserDTO } from '../../models/show-user.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(private readonly usersService: UsersService,
        configservice: ConfigService) {
        super({
          jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
          secretOrKey: configservice.jwtSecret,
          ignoreExpiration: false
        });
      }

    public async validate(payload: ShowUserDTO): Promise<ShowUserDTO> {
        const user = await this.usersService.findUser(
          payload.username,
        );

        if (!user) {
          throw new UnauthorizedException();
        }

        return user;
    }
}
