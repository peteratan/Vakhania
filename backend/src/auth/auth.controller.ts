import { Controller, Post, Delete, Body, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthUserDTO } from '../models/auth-user.dto';
import { AuthGuardWithBlacklisting } from '../common/guards/auth-blacklist.guard';
import { token } from '../common/decorators/token.decorator';

@Controller('session')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post()
    public async login(@Body() body: AuthUserDTO ) {
        return await this.authService.login(body);
    }

    @Delete()
    @UseGuards(AuthGuardWithBlacklisting)
    public async logoutUser(@token() theToken: string) {
         this.authService.blacklistToken(theToken);
         return { msg: 'Successful logout!' };
    }

}

