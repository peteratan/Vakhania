import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { AuthUserDTO } from '../models/auth-user.dto';
import { ShowUserDTO } from '../models/show-user.dto';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

    public constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
      ) {}
      public async login(user: AuthUserDTO): Promise<any> {
        const foundUser: ShowUserDTO = await this.usersService.findUser(user.credential);

        if (!foundUser) {
          throw new Error('Incorrect Username/Password!');
        }

        if (!(await this.usersService.validateUserPassword(user))) {
          throw new Error('Incorrect Password!');
        }

        const payload: ShowUserDTO = { ...foundUser };

        return {
          token: await this.jwtService.signAsync(payload),
        };
      }

      public blacklistToken(token: string): void {
        this.blacklist.push(token);
      }

      public isTokenBlacklisted(token: string): boolean {
        return this.blacklist.includes(token);
      }

}